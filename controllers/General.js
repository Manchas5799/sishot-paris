const vv = require('../config/var.json');
const ciudades = require('../models/mCiudad');
const pais = require('../models/mPais');
const persona = require('../models/mUsuario');
const tipoDocumento = require('../models/mTipoDocumento');
// const cliente = require('../models/mClientes');
const passport = require('passport');

/////////////////// MODELOS GRID-DATA

const tipoHabitacion = require('../models/mTipoHabitacion');
// const hotel = require('../models/mHotel');
const habitaciones = require('../models/mHabitacion');

function buscar(d, it) {
    let ans = {};
    for (let i = 0; i < d.length; i++) {
        if (d[i].id == it) {
            ans.nombre = d[i].nombre;
            ans.costo1 = d[i].costo1;
            break;
        }
    }
    return ans;
}
// const tipoExterno = false ;
// const mostrarVentanas = false;
module.exports = {
    async index(req, res) {
        // const estados = await new estadosHabitaciones().getAll();
        const tpH = await new tipoHabitacion().getAll();
        const habi = await new habitaciones().getAll();
        let datHabi = new Array();
        for (let i = 0; i < habi.length; i++) {
            datHabi[i] = {
                id: habi[i].id,
                // codigo : habi[i].codigo,
                costo: habi[i].TIPO_HABITACION_id,
                // estadoN: habi[i].ESTADOS_id,
                tipo: buscar(tpH, habi[i].TIPO_HABITACION_id),
                // estado: buscar(estados,habi[i].ESTADOS_id),
                imagen: habi[i].imagen
            }
        }
        res.render('./public', {
            layout: 'public',
            title: vv.development.APP + " | HOME",
            rooms: habi
        });
    },
    async nosotros(req, res) {
        const data = await new persona().getAll();
        let ex = [];
        for (let i = 0; i < data.length; i++) {
            ex[i] = data[i].nombres;
        }
        res.send(ex);
        // res.render('./public/nosotros',{
        //     layout: 'public',
        //     title: vv.development.APP  + " | NOSOTROS"
        // });
    },
    ///////////////////////////HABITACIONES
    habitaciones(req, res) {
        res.render('./public/habitaciones/habitaciones', {
            layout: 'public',
            title: vv.development.APP + " | HABITACIONES"
        });
    },
    async showHabitacion(req, res) {
        const { id } = req.params;
        let room = new habitaciones({ id });
        await room.getOne();
        console.log(room);
        res.render('./public/habitaciones/habitacion', {
            layout: 'public',
            room
        });
    },
    ///////////////////////////HABITACIONES
    galeria(req, res) {
        res.render('./public/galeria', {
            layout: 'public',
            title: vv.development.APP + " | GALERIA"
        });
    },
    contact(req, res) {
        res.render('./public/contact', {
            layout: 'public',
            title: vv.development.APP + " | CONTACTO"
        });
    },
    ////////////
    ////registro
    async registro(req, res) {
        const tpd = await new tipoDocumento().getAll();
        const paises = await new pais().getAll();
        res.render('./auth/registrar', {
            layout: false,
            paises: paises,
            tDoc: tpd
        });
    },
    async saveRegistro(req, res) {

        // res.send('work');
    },

    /////////
    /////ingreso
    login(req, res) {
        res.render('./auth/ingresar', {
            title: vv.development.APP + " | HOME",
            header: vv.development.APP,
            // mostrarVentanas
            layout: false,
            logo: "/public/img/logo_paris.png"
        });
    },
    async saveIngreso(req, res, next) {
        passport.authenticate('local.login', {
            successRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true
        })(req, res, next);
    },
    async logout(req, res) {
        req.session.reser = null;
        req.logout();
        res.redirect('/');
    },

    ////////////VISTAS
    async mCiudades(req, res) {
        const { pais } = req.params;
        console.log(pais);
        const ciud = new ciudades();
        const data = await ciud.getPorPais(pais);
        res.json(data);
    }
};