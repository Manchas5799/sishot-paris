const vv = require('../config/var.json');
const fs = require('fs');
const path = require('path');
const helpers = require('../libs/bcrypt');
const passport = require('passport');

const tipoDocumento = require('../models/mTipoDocumento');
const tipoServicio = require('../models/mTipoServicio');
const tipoAcceso =  require('../models/mTiposAcceso');
const tipoEmpleado = require('../models/mTipoEmpleado');
const tipoHabitacion = require('../models/mTipoHabitacion');

const estadosEmpleados = require('../models/mEstadosEmpleados');
const estadosHabitaciones = require('../models/mEstadosHabitaciones');
const estadosReservaciones = require('../models/mEstadosReservaciones');

const hotel = require('../models/mHotel');
const pais = require('../models/mPais');
const area = require('../models/mArea');

// const persona = require('../models/mUsuario');
const empleado = require('../models/mEmpleado');
const inventario = require('../models/mArticulo');
const habitaciones = require('../models/mHabitacion');


function buscar(d,it){
    let ans = {};
    for (let i = 0; i < d.length;i++) {
        if(d[i].id == it){
            ans = d[i].nombre;
            break;
        }
    }
    return ans;
}

module.exports = {
    index(req, res) {
        // res.json(req.session);
        res.render('./sistema/',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP,
            head : "SISTEMA"
        });
    },
    login(req, res) {
        res.render('./auth/ingresar',{
            title: vv.development.APP  + " | HOME",
            header: vv.development.APP,
            // mostrarVentanas
            layout: false,
            sistema: '/sistema',
            logo: "/public/img/logo_paris.png"
        });
    },
    async saveIngreso(req,res,next){
        passport.authenticate('local.sistema',{
            successRedirect: '/sistema',
            failureRedirect: '/sistema/login',
            failureFlash : true
        })(req,res,next);
    },
    ////////////////////////////////////////////
    //////INVENTARIO
    //////////////////////////////////
    async mInventario(req,res){
        const datos = await new inventario().getAll();
        res.render('./sistema/inventario/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | INVENTARIO',
            head: 'Registro al Sistema',
            accion : '/sistema/inventario/add',
            datos
        });
    },
    addInventario(req,res){
        res.render('./sistema/inventario/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | REGISTRO',
            head: 'Registro de Artículo',
            accion : '/sistema/inventario/add'
        });
    },
    async saveInventario(req,res){
        const data = req.body;
        let inv = new inventario(data);
        await inv.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect("/sistema/inventario/");
        // res.send("guardado");
    },
    async mArticulo(req, res) {
        const { id } = req.params;
        const dato = new inventario({id});
        await dato.getOne();

        res.render('./sistema/inventario/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | EDICION',
            head: 'Registro al Sistema',
            accion : '/sistema/inventario/'+req.params.id,
            dato
        });
        // res.send("articulo");
    },
    async updArticulo(req, res) {
        const data = req.body;
        let inv = new inventario({id:req.params.id});
        await inv.actualizar(data);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect("/sistema/inventario/");
    },
    async delArticulo(req, res) {
        await new inventario({
            id: req.params.id
        }).eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect("/sistema/inventario/");
    },
    async fInventario(req, res) {
        const { filtro } = req.params;
        const { fill } = req.body;
        const data = await new inventario({},{
            0: filtro,
            1: fill
        }).filter();
        res.json(data);
    },
    ////////////////////////////////////////////
    //////HABITACIONES
    //////////////////////////////////
    async mHabitaciones(req,res){
        const hoteles = await new hotel().getAll();
        const estados = await new estadosHabitaciones().getAll();
        const tpH = await new tipoHabitacion().getAll();
        const habi = await new habitaciones().getAll();
        let datHAbi = new Array();
        for(let i = 0; i < habi.length;i++){
            datHAbi[i] = {
                id: habi[i].id,
                codigo : habi[i].codigo,
                tipoN: habi[i].TIPO_HABITACION_id,
                estadoN: habi[i].ESTADOS_id,
                tipo : buscar(tpH,habi[i].TIPO_HABITACION_id),
                estado: buscar(estados,habi[i].ESTADOS_id)
            }
        }
        res.render('./sistema/habitaciones/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/habitaciones/add',
            hoteles : hoteles,
            estado_habitaciones :  estados,
            tipo_habitaciones : tpH,
            datos : habi,
            head: "HABITACIONES"
        });
    },
    async addHabitaciones(req, res) {
        const hoteles = await new hotel().getAll();
        const estados = await new estadosHabitaciones().getAll();
        const tpH = await new tipoHabitacion().getAll();
        res.render('./sistema/habitaciones/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/habitaciones/add',
            hoteles : hoteles,
            estado_habitaciones :  estados,
            tipo_habitaciones : tpH,
            head: "HABITACIONES"
        });
    },
    async saveHabitaciones(req, res) {
        const { hotel,tipoH,codigo,estadoH} = req.body;
        const oPath = req.file.path;
        const oName = req.file.originalname;
        const ext = path.extname(oName);
        const dest = path.resolve('public/storage/'+codigo+ext);
        fs.renameSync(oPath,dest);
        const data = {
            HOTEL_id : hotel,
            TIPO_HABITACION_id : tipoH,
            codigo : codigo,
            ESTADOS_id : estadoH,
            imagen : codigo+ext
        };
        console.log(req.file);
        let habi = new habitaciones(data);
        await habi.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/habitaciones');
    },
    async mHabitacion(req, res) {
        const hoteles = await new hotel().getAll();
        const estados = await new estadosHabitaciones().getAll();
        const tpH = await new tipoHabitacion().getAll();
        const habi = new habitaciones({id:req.params.id});
        await habi.getOne();
        // {
        //         id: habi.id,
        //         codigo : habi.codigo,
        //         tipoN: habi.TIPO_HABITACION_id,
        //         estadoN: habi.ESTADOS_id,
        //         hotelN : habi.HOTEL_id,
        //         hotel: buscar(hoteles,habi.HOTEL_id),
        //         tipo : buscar(tpH,habi.TIPO_HABITACION_id),
        //         estado: buscar(estados,habi.ESTADOS_id),
        //         imagen: habi.imagen
        //     }
        res.render('./sistema/habitaciones/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/habitaciones/'+req.params.id,
            hoteles : hoteles,
            estado_habitaciones :  estados,
            tipo_habitaciones: tpH,
            head: "HABITACIONES",
            dato: habi
        });
    },
    async updHabitacion(req, res) {
        const { hotel,tipoH,codigo,estadoH} = req.body;
        let nImg = '';
        let data = {
            HOTEL_id : hotel,
            TIPO_HABITACION_id : tipoH,
            codigo : codigo,
            ESTADOS_id : estadoH
        };
        if(req.file){
            const oPath = req.file.path;
            const oName = req.file.originalname;
            const ext = path.extname(oName);
            const dest = path.resolve('public/storage/'+codigo+ext);
            fs.renameSync(oPath,dest);
            nImg = codigo+ext;
            data = {
                HOTEL_id : hotel,
                TIPO_HABITACION_id : tipoH,
                codigo : codigo,
                ESTADOS_id : estadoH,
                imagen : nImg
            };
        }
        console.log(data);
        // console.log(req.file);
        let nH = new habitaciones({id:req.params.id});
        await nH.actualizar(data);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/habitaciones');
    },
    async delHabitacion(req, res) {
        await new habitaciones({id:req.params.id}).eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/habitaciones');
    },
    fHabitaciones(req, res) {
        res.send("filtrado");
    },
    ////////////////////////////////////////////
    //////CLIENTES
    //////////////////////////////////
    
    //////////////////////////////////////
    //////EMPLEADOS
    //////////
    async Empleados(req,res){
        const pers = new empleado();
        const datos = await pers.inner();
        
        const paises = await new pais().getAll();
        const tD = await new tipoDocumento().getAll();
        const areas = await new area().getAll();
        const estadoEmpleado = await new estadosEmpleados().getAll();
        const tipoEmp = await new tipoEmpleado().getAll();
        console.log(datos);
        res.render('./sistema/empleados/crud',{
            crear : true,
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | TODOS',
            head: 'Registro al Sistema',
            accion : '/sistema/empleados/add',
            paises : paises,
            tD : tD,
            datos: datos,
            areas : areas,
            estadoEmp : estadoEmpleado,
            tipoEmp : tipoEmp
        });  
    },
    async addEmpleados(req, res) {
        const paises = await new pais().getAll();
        const tD = await new tipoDocumento().getAll();
        const areas = await new area().getAll();
        const estadoEmpleado = await new estadosEmpleados().getAll();
        const tipoEmp = await new tipoEmpleado().getAll();
        console.log(paises[0]);
        res.render('./sistema/empleados/crud',{
            crear : true,
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | REGISTRO',
            head: 'Registro al Sistema',
            accion : '/sistema/empleados/add',
            paises : paises,
            tD : tD,
            areas : areas,
            estadoEmp : estadoEmpleado,
            tipoEmp : tipoEmp
        });  
    },
    async saveEmpleados(req, res) {
        console.log(req.body);
        const datosPersona = { id : req.body.id,
                                nombres : req.body.nombres,
                                apellidos : req.body.apellidos,
                                correo : req.body.correo,
                                telefono : req.body.telefono,
                                sexo : req.body.sexo,
                                TIPO_DOCUMENTO_id : req.body.TIPO_DOCUMENTO_id,
                                documento : req.body.documento,
                                password : req.body.password,
                                CIUDAD_PAIS_id : req.body.CIUDAD_PAIS_id,
                                CIUDAD_id  : req.body.CIUDAD_id};
        datosPersona.password = await helpers.encryptPassword(datosPersona.password);
        // console.log(datosPersona);
        // const data = await new persona(datosPersona).guardar();
        const datosEmple = {
            "TIPO_EMPLEADO_id" : req.body.TIPO_EMPLEADO_id,
            "ESTADOS_id" : req.body.ESTADOS_id,
            "AREA_id" : req.body.AREA_id
        };
        const empe = new empleado(datosEmple,datosPersona);
        let data2 = await empe.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        console.log("EL EMPLEADO RETORNO ESTO ----->>> ",data2);
        res.redirect('/sistema/empleados');
        // res.redirect('/sistema/empleados/'+data);
    },
    async mEmpleados(req, res) {
        const { id } = req.params;
        const data = new empleado({},{id});
        await data.getOne();
        console.log(data);
        const areas = await new area().getAll();
        const paises = await new pais().getAll();
        const tipoEmp = await new tipoEmpleado().getAll();
        const tD = await new tipoDocumento().getAll();
        const estadoEmpleado = await new estadosEmpleados().getAll();
        console.log(data);
        res.render('./sistema/empleados/crud',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            title: vv.development.APP + ' | EDICION',
            head: 'Registro al Sistema',
            accion : '/sistema/empleados/'+req.params.id,
            paises : paises,
            areas : areas,
            tD : tD,
            tipoEmp : tipoEmp,
            estadoEmp : estadoEmpleado,
            dato: data
        });
    },
    async updEmpleados(req, res) {
        const datosPersona = { 
            id : req.params.id,
            nombres : req.body.nombres,
            apellidos : req.body.apellidos,
            correo : req.body.correo,
            telefono : req.body.telefono,
            sexo : req.body.sexo,
            TIPO_DOCUMENTO_id : req.body.TIPO_DOCUMENTO_id,
            documento : req.body.documento,
            password : await helpers.encryptPassword(req.body.password),
            CIUDAD_PAIS_id : req.body.CIUDAD_PAIS_id,
            CIUDAD_id  : req.body.CIUDAD_id};
        const datosEmple = {
            TIPO_EMPLEADO_id : req.body.TIPO_EMPLEADO_id,
            ESTADOS_id : req.body.ESTADOS_id,
            AREA_id : req.body.AREA_id
        };
        let emp = new empleado({},{id: req.params.id});
        await emp.actualizar(datosEmple,datosPersona);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        // const data2 =  await new empleado(datosEmple,{
        //     PERSONA_id : req.params.id
        // }).actualizar();
        // await new persona(req.body,{
        //     id : req.params.id
        // }).actualizar();
        res.redirect('/sistema/empleados/');
    },
    async delEmpleados(req, res) {
        const { id }= req.params;
        const ans = await new empleado("",{id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/empleados/');
    },
    fEmpleados(req, res) {
        res.send("filtrado");
    },
    /////////////////////////////
    ///SISTEMA
    ////////////////
    ////////////
    ////TIPO HABITACION
    async tipoHabitacion(req,res){
        // res.send("s");
        const datos = await new tipoHabitacion().getAll();
        res.render('./sistema/configuraciones/tipoHabitacion',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoHabitacion/add',
            datos: datos,
            title: vv.development.APP + ' | TODOS',
            head: "TIPO HABITACION"
        });
    },
    async addTipoHabitacion(req,res){
        // const datos = await new tipoHabitacion().getTodo();
        res.render('./sistema/configuraciones/tipoHabitacion',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoHabitacion/add',
            title: vv.development.APP + ' | REGISTRO'
        });
    },
    async saveTipoHabitacion(req,res){
        await new tipoHabitacion(req.body).guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoHabitacion/');
        // res.redirect('./sistema/configuracion/tipoHabitacion/');
    },
    async mTipoHabitacion(req,res){
        const { id } = req.params;
        const datos = new tipoHabitacion({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/tipoHabitacion/'+datos);
        // console.log(JSON.parse(datos));
        // console.log(datos);
        if(datos){
            res.render('./sistema/configuraciones/tipoHabitacion',{
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                dato : datos,
                accion: `/sistema/configuracion/tipoHabitacion/${id}`,
                title: vv.development.APP + ' | EDICION',
                head: "TIPO HABITACION"
            });
        }
    },
    async updTipoHabitacion(req,res){
        const { id } =  req.params;
        let th = new tipoHabitacion({id});
        await th.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoHabitacion');
    },
    async delTipoHabitacion(req,res){
        const { id }= req.params;
        const ans = new tipoHabitacion({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoHabitacion');
    },

    ////////////
    ////TIPO DOCUMENTO
    async tipoDocumento(req,res){
        // res.send("s");
        const datos = await new tipoDocumento().getAll();
        res.render('./sistema/configuraciones/tipoDocumento',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoDocumento/add',
            datos: datos,
            title: vv.development.APP + ' | TODOS',
            head: "TIPO DOCUMENTO"
        });
    },
    addTipoDocumento(req,res){
        res.render('./sistema/configuraciones/tipoDocumento',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoDocumento/add',
            title: vv.development.APP + ' | REGISTRO'
        });
    },
    async saveTipoDocumento(req,res){
        const { nombre } = req.body;
        const tD = new tipoDocumento({nombre});
        await tD.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoDocumento/');
        // res.redirect('./sistema/configuracion/tipoDocumento/');
    },
    async mTipoDocumento(req,res){
        const { id } = req.params;
        const datos = new tipoDocumento({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/tipoDocumento/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/tipoDocumento',{
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.nombre,
                accion: `/sistema/configuracion/tipoDocumento/${id}`,
                title: vv.development.APP + ' | EDICION',
                head: "TIPO DOCUMENTO"
            });
        }
    },
    async updTipoDocumento(req,res){
        const { id } =  req.params;
        const { nombre } = req.body;
        console.log(id,nombre);
        let tD = new tipoDocumento({
            id : id
        });
        await tD.actualizar({nombre});
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoDocumento');
    },
    async delTipoDocumento(req,res){
        const { id }= req.params;
        const ans = new tipoDocumento({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoDocumento');
    },

    ////////////
    ////TIPO SERVICIO
    async tipoServicio(req,res){
        // res.send("s");
        const datos = await new tipoServicio().getAll();
        res.render('./sistema/configuraciones/tipoServicio',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoServicio/add',
            datos: datos,
            head: "TIPO SERVICIO"
        });
    },
    addTipoServicio(req,res){
        res.render('./sistema/configuraciones/tipoServicio',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoServicio/add'
        });
    },
    async saveTipoServicio(req,res){
        const { nombre,precio } = req.body;
        const tS = new tipoServicio({
            nombre : nombre,precio: precio});
        await tS.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoServicio/');
        // res.redirect('./sistema/configuracion/tipoServicio/');
    },
    async mTipoServicio(req,res){
        const { id } = req.params;
        const datos = new tipoServicio({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/tipoServicio/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/tipoServicio',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.nombre,
                precio : datos.precio,
                accion: `/sistema/configuracion/tipoServicio/${id}`,
                head: "TIPO SERVICIO"
            });
        }
    },
    async updTipoServicio(req,res){
        const { id } = req.params;
        // console.log(id,nombre);
        let tS = new tipoServicio({id});
        await tS.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoServicio');
    },
    async delTipoServicio(req,res){
        const { id }= req.params;
        const ans = new tipoServicio({
            id : id
        });
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoServicio');
    },
    ////////////
    ////TIPO Acceso
    async tipoAcceso(req,res){
        // res.send("s");
        const datos = await new tipoAcceso().getAll();
        res.render('./sistema/configuraciones/tipoAcceso',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoAcceso/add',
            datos: datos,
            title: vv.development.APP + ' | TODOS',
            head: "TIPO ACCESO"
        });
    },
    addTipoAcceso(req,res){
        res.render('./sistema/configuraciones/tipoAcceso',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoAcceso/add',
            title: vv.development.APP + ' | REGISTRO'
        });
    },
    async saveTipoAcceso(req,res){
        const { nombre } = req.body;
        const tA = new tipoAcceso({acceso:nombre});
        await tA.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoAcceso/');
        // res.redirect('./sistema/configuracion/tipoAcceso/');
    },
    async mTipoAcceso(req,res){
        const { id } = req.params;
        const datos = new tipoAcceso({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/tipoAcceso/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/tipoAcceso',{
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.acceso,
                accion: `/sistema/configuracion/tipoAcceso/${id}`,
                title: vv.development.APP + ' | EDICION',
                head: "TIPO ACCESO"
            });
        }
    },
    async updTipoAcceso(req,res){
        const { id } = req.params;
        const { nombre } = req.body;
        // console.log(id,nombre);
        let tA = new tipoAcceso({id});
        await tA.actualizar({acceso: nombre});
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoAcceso');
    },
    async delTipoAcceso(req,res){
        const { id }= req.params;
        const ans = new tipoAcceso({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoAcceso');
    },
    ////////////
    ////TIPO Empleado
    async tipoEmpleado(req,res){
        // res.send("s");
        const datos = await new tipoEmpleado().getAll();
        res.render('./sistema/configuraciones/tipoEmpleado',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoEmpleado/add',
            datos: datos,
            head: "TIPO EMPLEADO"
        });
    },
    addTipoEmpleado(req,res){
        res.render('./sistema/configuraciones/tipoEmpleado',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/tipoEmpleado/add'
        });
    },
    async saveTipoEmpleado(req,res){
        const lastId = new tipoEmpleado(req.body);
        await lastId.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoEmpleado/');
        // res.redirect('./sistema/configuracion/tipoEmpleado/');
    },
    async mTipoEmpleado(req,res){
        const { id } = req.params;
        const datos = new tipoEmpleado({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/tipoEmpleado/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/tipoEmpleado',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                dato: datos,
                accion: `/sistema/configuracion/tipoEmpleado/${id}`,
                head: "TIPO EMPLEADO"
            });
        }
    },
    async updTipoEmpleado(req,res){
        const { id } = req.params;
        // const { nombre } = req.body;
        // console.log(id,nombre);
        let tE = new tipoEmpleado({id});
        await tE.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoEmpleado/'+id);
    },
    async delTipoEmpleado(req,res){
        const { id }= req.params;
        const ans = new tipoEmpleado({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/tipoEmpleado');
    },
    ////////////
    ////hotel
    async hotel(req,res){
        // res.send("s");
        const paises = await new pais().getAll();
        const datos = await new hotel().getAll();
        res.render('./sistema/configuraciones/hotel',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/hotel/add',
            datos: datos,
            title: vv.development.APP + ' | TODOS',
            paises: paises,
            head: "HOTEL"
        });
    },
    async addHotel(req,res){
        const paises = await new pais().getAll();
        res.render('./sistema/configuraciones/hotel',{
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/hotel/add',
            title: vv.development.APP + ' | REGISTRO',
            paises: paises
        });
    },
    async saveHotel(req,res){
        const lastId = new hotel(req.body);
        await lastId.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/hotel/');
        // res.redirect('./sistema/configuracion/hotel/');
    },
    async mHotel(req,res){
        const { id } = req.params;
        const paises = await new pais().getAll();
        const datos = new hotel({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/hotel/'+datos);
        // console.log(JSON.parse(datos));
        console.log(datos);
        if(datos){
            res.render('./sistema/configuraciones/hotel',{
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                dato: datos,
                accion: `/sistema/configuracion/hotel/${id}`,
                title: vv.development.APP + ' | EDICION',
                paises: paises,
                head: "HOTEL"
            });
        }
    },
    async updHotel(req,res){
        const { id } = req.params;
        // const { nombre } = req.body;
        // console.log(id,nombre);
        const h = new hotel({id});
        await h.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/hotel');
    },
    async delHotel(req,res){
        const { id }= req.params;
        const ans = new hotel({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/hotel');
    },
    ////////////
    ////TIPO area
    async area(req,res){
        // res.send("s");
        const tAcces = await new tipoAcceso().getAll();
        const datos = await new area().getAll();
        res.render('./sistema/configuraciones/area',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/area/add',
            datos: datos,
            tipos: tAcces,
            head: "AREA"
        });
    },
    async addArea(req,res){
        const tAcces = await new tipoAcceso().getAll();
        res.render('./sistema/configuraciones/area',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/area/add',
            tipos: tAcces
        });
    },
    async saveArea(req,res){
        // const { nombre } = req.body;
        const lastId = new area(req.body);
        await lastId.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/area/');
        // res.redirect('./sistema/configuracion/area/');
    },
    async mArea(req,res){
        const tAcces = await new tipoAcceso().getAll();
        const { id } = req.params;
        const datos = new area({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/area/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/area',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                dato: datos,
                accion: `/sistema/configuracion/area/${id}`,
                tipos: tAcces,
                head: "AREA"
            });
        }
    },
    async updArea(req,res){
        const { id } = req.params;
        const { nombre } = req.body;
        console.log(id,nombre);
        const aarea = new area({id});
        await aarea.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/area');
    },
    async delArea(req,res){
        const { id }= req.params;
        const ans = new area({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/area');
    },
    
    ////////////
    ////TIPO estadosEmpleados
    async estadosEmpleados(req,res){
        // res.send("s");
        const datos = await new estadosEmpleados().getAll();
        res.render('./sistema/configuraciones/estadosEmpleados',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosEmpleados/add',
            datos: datos,
            head: "ESTADOS EMPLEADOS"
        });
    },
    addEstadosEmpleados(req,res){
        res.render('./sistema/configuraciones/estadosEmpleados',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosEmpleados/add'
        });
    },
    async saveEstadosEmpleados(req,res){
        const { nombre } = req.body;
        const lastId = new estadosEmpleados({nombre});
        await lastId.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosEmpleados/');
        // res.redirect('./sistema/configuracion/estadosEmpleados/');
    },
    async mEstadosEmpleados(req,res){
        const { id } = req.params;
        const datos = new estadosEmpleados({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/estadosEmpleados/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/estadosEmpleados',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.nombre,
                accion: `/sistema/configuracion/estadosEmpleados/${id}`,
                head: "ESTADOS EMPLEADOS"
            });
        }
    },
    async updEstadosEmpleados(req,res){
        const { id } = req.params;
        const { nombre } = req.body;
        console.log(id,nombre);
        let eE = new estadosEmpleados({id});
        await eE.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosEmpleados/');
    },
    async delEstadosEmpleados(req,res){
        const { id }= req.params;
        const ans = await new estadosEmpleados({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosEmpleados');
    },
    /////////////
    ////IMPLEMENTAR LAS DE HABITACIONE Y LAS DE RESERVACIONES
    ////////////
    ////TIPO estadosHabitaciones
    async estadosHabitaciones(req,res){
        // res.send("s");
        const datos = await new estadosHabitaciones().getAll();
        res.render('./sistema/configuraciones/estadosHabitaciones',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosHabitaciones/add',
            datos: datos,
            head: "ESTADOS HABITACIONES"
        });
    },
    addEstadosHabitaciones(req,res){
        res.render('./sistema/configuraciones/estadosHabitaciones',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosHabitaciones/add'
        });
    },
    async saveEstadosHabitaciones(req,res){
        const { nombre } = req.body;
        const eH = new estadosHabitaciones({nombre});
        await eH.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosHabitaciones/');
        // res.redirect('./sistema/configuracion/estadosHabitaciones/');
    },
    async mEstadosHabitaciones(req,res){
        const { id } = req.params;
        const datos = new estadosHabitaciones({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/estadosHabitaciones/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/estadosHabitaciones',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.nombre,
                accion: `/sistema/configuracion/estadosHabitaciones/${id}`,
                head: "ESTADOS HABITACIONES"
            });
        }
    },
    async updEstadosHabitaciones(req,res){
        const { id } = req.params;
        // console.log(id,nombre);
        let eH = new estadosHabitaciones({id});
        await eH.actualizar(req.body);
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosHabitaciones');
    },
    async delEstadosHabitaciones(req,res){
        const { id }= req.params;
        const ans = new estadosHabitaciones({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosHabitaciones');
    },
    ////////////
    ////TIPO estadosReservaciones
    async estadosReservaciones(req,res){
        // res.send("s");
        const datos = await new estadosReservaciones().getAll();
        res.render('./sistema/configuraciones/estadosReservaciones',{
            title: vv.development.APP + ' | TODOS',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosReservaciones/add',
            datos: datos,
            head: "ESTADOS RESERVACIONES"
        });
    },
    addEstadosReservaciones(req,res){
        res.render('./sistema/configuraciones/estadosReservaciones',{
            title: vv.development.APP + ' | REGISTRO',
            tipoUsuario : {
                exist : true,
                nombre : "INICIO"
            },
            accion: '/sistema/configuracion/estadosReservaciones/add'
        });
    },
    async saveEstadosReservaciones(req,res){
        const { nombre } = req.body;
        const eR = new estadosReservaciones({nombre});
        await eR.guardar();
        req.flash('exitoso','Guardado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosReservaciones/');
        // res.redirect('./sistema/configuracion/estadosReservaciones/');
    },
    async mEstadosReservaciones(req,res){
        const { id } = req.params;
        const datos = new estadosReservaciones({id});
        await datos.getOne();
        // res.redirect('/sistema/configuracion/estadosReservaciones/'+datos);
        // console.log(JSON.parse(datos));
        if(datos){
            res.render('./sistema/configuraciones/estadosReservaciones',{
                title: vv.development.APP + ' | EDICION',
                tipoUsuario : {
                    exist : true,
                    nombre : "INICIO"
                },
                id : datos.id,
                nombre : datos.nombre,
                accion: `/sistema/configuracion/estadosReservaciones/${id}`,
                head: "ESTADOS RESERVACIONES"
            });
        }
    },
    async updEstadosReservaciones(req,res){
        const { id } = req.params;
        const { nombre } = req.body;
        // console.log(id,nombre);
        let eR = new estadosReservaciones({id});
        await eR.actualizar({nombre});
        req.flash('exitoso','Actualizado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosReservaciones');
    },
    async delEstadosReservaciones(req,res){
        const { id }= req.params;
        const ans = new estadosReservaciones({id});
        await ans.eliminar();
        req.flash('exitoso','Eliminado Satisfactoriamente');
        res.redirect('/sistema/configuracion/estadosReservaciones');
    },
};