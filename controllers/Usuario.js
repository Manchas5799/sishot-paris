const Reserva = require('../models/mReserva');
const Detalle = require('../models/mDetalleReserva');
const Habitacion = require('../models/mHabitacion');
const DetalleFactura = require('../models/mDetalleComprobante');
const Factura = require('../models/mComprobante');
const moments = require('moment');
module.exports = {
    index(req, res) {
        let reser = new Reserva(req.session.reser ? { oldDatos: req.session.reser } : { oldDatos: null });
        console.log(reser.totalDetalles);
        if (reser.totalDetalles > 0) {
            let total = 0;
            for (let i = 0; i < reser.totalDetalles; i++) {
                total += reser.detalles[i].subTotal;
            }
            res.render('./public/reserva/reservas', {
                layout: 'public',
                detalles: reser.detalles,
                total, detalle_compra: true

            });
        } else {
            req.flash('fallido', 'Añada Habitaciones a sus posibles reservas');
            res.redirect('/');
        }

    },
    async add(req, res) {
        let current_datetime = new Date();
        let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate();
        let reser = new Reserva(req.session.reser ? { oldDatos: req.session.reser } : { oldDatos: null, fechaReservacion: formatted_date });
        // let reser = req.session.reser?req.session.reser:new Reserva({
        //     fechaReservacion: formatted_date
        // });

        await reser.setCliente(req.user.id);

        console.log(req.body);
        let { fecha_inicio, fecha_fin } = req.body;
        fecha_inicio = moments(fecha_inicio);
        fecha_fin = moments(fecha_fin);
        const dias = fecha_fin.diff(fecha_inicio, 'days');
        let room = new Habitacion({ id: req.body.habitacion_id });
        await room.getOne();
        let subTotal = room.tipoHabitacion.costo1 * dias;
        // console.log(subTotal);
        let detalle_reservacion = new Detalle({
            fecha_inicio: fecha_inicio._i,
            fecha_fin: fecha_fin._i,
            dias,
            subTotal
        });
        await detalle_reservacion.setHabitaciones(req.body.habitacion_id);
        reser.addDetalle(detalle_reservacion);
        console.log(detalle_reservacion);
        console.log(reser);
        let total = 0;
        for (let i = 0; i < reser.detalles.length; i++) {
            total += reser.detalles[i].subTotal;
        }
        req.session.reser = reser;
        // res.json(reser.detalles);
        res.render('./public/reserva/reservas', {
            layout: 'public',
            detalles: reser.detalles,
            total,
            detalle_compra: true
        });
    },
    remover(req, res) {
        let x = req.params.id;
        let reser = new Reserva(req.session.reser ? { oldDatos: req.session.reser } : { oldDatos: null });
        reser.removerDetalle(x);
        req.session.reser = reser;
        res.redirect('/usuario/carrito');
    },
    reset(req, res) {
        let reser = new Reserva({ oldDatos: req.session.reser }).reset();
        req.session.reser = reser;
        res.redirect('/');
    },
    async reservar(req, res) {
        let reser = new Reserva(req.session.reser ? { oldDatos: req.session.reser } : { oldDatos: null });
        let total = 0;
        for (let i = 0; i < reser.detalles.length; i++) {
            total += reser.detalles[i].subTotal;
        }
        let detalleFac = new DetalleFactura({
            subtotal: total,
            fecha: reser.fechaReservacion
        }, reser);
        let Fac = new Factura({
            total: total,
            fecha: reser.fechaReservacion
        });
        let reservava = new Reserva({
            oldDatos: reser
        });
        let iddReser = await reservava.guardar();
        reservava.setId(iddReser);
        await Fac.setCliente(req.user.id);
        let idd = await Fac.guardar();
        await detalleFac.setFactura(idd);
        await detalleFac.setReserva(iddReser);
        await detalleFac.guardar();
        req.session.reser = null;
        res.redirect('/usuario/reservas');
    },
    async showAll(req, res) {
        let datos = await new Reserva().inner(req.user.id);
        if (datos.length > 0) {
            let hab_reser = [];
            for (let i = 0; i < datos.length; i++) {
                let px = new Detalle({ id: datos[i].id });
                await px.getOne();
                hab_reser[i] = px;
            }
            // res.json(hab_reser);
            res.render('./public/reserva/pedidos', {
                detalle_compra: false,
                layout: 'public',
                detalles: hab_reser
            });
        } else {
            req.flash('fallido', 'No cuenta con habitaciones en reserva');
            res.redirect('/');
        }

    }
};