//CREACION DE APP
const express = require('express');
const config = require('./server/config.js');

//CONFIGURACIONES
const app = config(express());
app.listen(app.get('port'), () => {
    console.log("Servidor iniciado ", app.get('port'));
});