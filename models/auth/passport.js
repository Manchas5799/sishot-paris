const passport = require('passport');
const strat = require('passport-local').Strategy;
const persona = require('../mUsuario');
const cliente = require('../mClientes');
const empleado = require('../mEmpleado');
const helpers = require('../../libs/bcrypt');


/////LOGIN CLIENTES
passport.use('local.sistema', new strat({
    usernameField: 'documento',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const pers = new empleado();
    const datos = await pers.inner();
    for (let i = 0; i < datos.length; i++) {
        if (datos[i].documento == username) {
            if (await helpers.checkPassword(password, datos[i].password)) {
                // req.session.nombre = datos[i].nombres;
                // // console.log("NOMBRE DE EL USUARIO ----> ",req.session.nombre);
                // // console.log("ID DE SESSION -->" ,req.session.nombres);
                // answer = true;
                // console.log(datos[i]);
                done(null, datos[i], req.flash('exitoso', 'Ingreso Satisfactorio ' + datos[i].nombres));
            }
        }
    }


    return done(null, false, req.flash('fallido', 'Ingreso Fallido'));
}));
//////////////////////LOGIN
passport.use('local.login', new strat({
    usernameField: 'documento',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const pers = new cliente();
    let datos = await pers.inner();
    // const emp = new empleado();
    // const datas = await emp.inner();
    // datos = datos.concat(datas);
    for (let i = 0; i < datos.length; i++) {
        if (datos[i].documento == username) {
            if (await helpers.checkPassword(password, datos[i].password)) {
                // req.session.nombre = datos[i].nombres;
                // // console.log("NOMBRE DE EL USUARIO ----> ",req.session.nombre);
                // // console.log("ID DE SESSION -->" ,req.session.nombres);
                // answer = true;
                // console.log(datos[i]);
                done(null, datos[i], req.flash('exitoso', 'Ingreso Satisfactorio ' + datos[i].nombres));
            }
        }
    }


    return done(null, false, req.flash('fallido', 'Ingreso Fallido'));
}));


////////////////////REGISTRANDO
passport.use('local.registro', new strat({
    usernameField: 'documento',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    // if(req.some(e=>{
    //     return (e == 'null' || 
    // }))
    const datosPersona = {
        id: req.body.id,
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        correo: req.body.correo,
        telefono: req.body.telefono,
        sexo: req.body.sexo,
        TIPO_DOCUMENTO_id: req.body.tipoDoc,
        documento: req.body.documento,
        password: req.body.password,
        CIUDAD_PAIS_id: req.body.pais,
        CIUDAD_id: req.body.ciudad
    };
    // console.log(datosPersona);
    datosPersona.password = await helpers.encryptPassword(datosPersona.password);
    const pers = new persona(datosPersona);
    let data = null;
    let ans = null;
    try {
        data = await pers.guardar();
        const cli = new cliente({
            PERSONA_id: data
        });
        try {
            await cli.guardar();
            datosPersona.id = data;
            ans = done(null, datosPersona, req.flash('exitoso', 'Guardado'));
        } catch (error) {
            console.log("error 2 catch");
        }
        console.log("data -->", data);
    } catch (error) {
        console.log("error 1 catch");
        // console.log(error);
        if (error.code == "ER_DUP_ENTRY") {
            console.log("error code --> " + error.code);
            ans = done(null, false, req.flash('fallido', 'Registro Duplicado'));
        }
    }


    
    // if(data.code != "ER_DUP_ENTRY"){
    //     const cli = new cliente({
    //         PERSONA_id: data
    //     });
    //     await cli.guardar().catch((e) => {
    //         if (e) {
    //             return null;
    //         }
    //     });
    //     datosPersona.id = data;
    //     // console.log('saved');
    //     return done(null, datosPersona, req.flash('exitoso', 'Guardado'));
    // }else{
    //     return done(null, false, req.flash('fallido', 'Registro Duplicado'));
    // }
    // return done(null, false, req.flash('fallido', 'Registro fallido'));
    return ans;
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});
passport.deserializeUser(async (id, done) => {
    const d = new persona({ id: id });
    await d.getOne();

    done(null, d);
});