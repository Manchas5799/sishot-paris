const bd = require('../server/bd');
class Main{
    constructor(table_name = "",datos = "", condition = "",datos_soli = "*",tables_adi = ""){
        this.table_name = table_name;
        this.datos = datos;
        this.condition = condition;
        this.datos_soli = datos_soli;
        this.tables_adi = tables_adi; 
    }
    async guardar(){
        let sql = `INSERT INTO ${this.table_name} SET ?`;
        
        let data = null;
        console.log(this.datos);
        try {
            let li = await bd.query(sql,[this.datos]);
            data = li.insertId;
        } catch (error) {
            console.log("errror" + error);
           throw error; 
        }
        // const data = await bd.query(sql,[this.datos]).catch((e)=>{
        //     if(e){
        //         console.log("ACCION INVALIDA EN LA BD");
        //         console.log(e);
        //         ans = e;
        //     }
            
        // });

        return data;
    }
    async getOne(){
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} WHERE ?`;
        console.log(sql);
        const datos = await bd.query(sql,[this.condition]).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(datos);
        return datos[0];
    }
    async actualizar(){
        let sql = `UPDATE ${this.table_name} set ? WHERE ?`;
        console.log(sql);
        const datos = await bd.query(sql,[this.datos,this.condition]).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(datos);
    }
    async getAll(){
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name}`;
        if(this.condition != ''){
            sql+= ' WHERE ?';
        }
        console.log(sql);
        const data = await bd.query(sql,[this.condition]).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(data);   
        return data; 
    }
    // generarQuery(com){

    // }
    async eliminar(){
        let sql = `DELETE FROM ${this.table_name} WHERE ?`;
        const data = await bd.query(sql,[this.condition]).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });

        console.log(data);
        return data;
    }
    async inner(){
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} as tabla1 INNER JOIN ${this.tables_adi} as tabla2 WHERE ${this.condition}`;
        console.log(sql);
        const data = await bd.query(sql).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(data);
        return data;
    }
    async filter(){
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} WHERE ${this.condition[1]} like '%${this.condition[0]}%'`;
        console.log(sql);
        const data = await bd.query(sql).catch((e)=>{
            if(e){
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(data);
        return data;
    }
}
module.exports = Main;