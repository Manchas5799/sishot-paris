const bd = require('../server/bd');
const main = require('.')
class Articulo{
    constructor(datos = ""){
        //super('articulos',datos,condition,datos_soli,tables_adi);
        this.table_name = 'articulos';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.fecha_adquisicion = datos.fecha_adquisicion;
        this.precio_adquisicion = datos.precio_adquisicion;
        this.descripcion = datos.descripcion;
        this.codigo = datos.codigo;
        this.imagen = datos.imagen;
    }
    async getAll(){
    	let datos = await new main(this.table_name).getAll();
    	let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new Articulo({
				id : datos[i].id,
				nombre : datos[i].nombre,
				fecha_adquisicion : datos[i].fecha_adquisicion,
				precio_adquisicion : datos[i].precio_adquisicion,
				descripcion : datos[i].descripcion,
				codigo : datos[i].codigo,
				imagen: datos[i].imagen
			});
		}
		return objs;
    }
    async getOne(){
        let con = new main(this.table_name,{},{id : this.id});
        let dato = await con.getOne();
        this.setid(dato.id);
        this.setnombre(dato.nombre);
        this.setFechaAdquisicion(dato.fecha_adquisicion);
        this.setPrecioAdquisicion(dato.precio_adquisicion);
        this.setDescripcion(dato.descripcion);
        this.setCodigo(dato.codigo);
        this.setImagen(dato.imagen);
    }
    async eliminar(){
        let con = new main(this.table_name,{},{id : this.id});
        await con.eliminar();
    }
    async actualizar(datos){
        let con = new main(this.table_name,datos,{id : this.id});
        await con.actualizar(); 
    }
    setid(id){
        this.id = id;
    }
    setnombre(nombre){
        this.nombre = nombre;
    }
    setFechaAdquisicion(fecha){
    	this.fecha_adquisicion = fecha;
    }
    setPrecioAdquisicion(precio){
    	this.precio_adquisicion = precio;
    }
    setDescripcion(des){
    	this.descripcion = des;
    }
    setCodigo(cod){
    	this.codigo = cod;
    }
    setImagen(img){
    	this.imagen =img;
    }
    async guardar(){
        return await new main(this.table_name,{
            nombre : this.nombre,
            fecha_adquisicion: this.fecha_adquisicion,
            precio_adquisicion : this.precio_adquisicion,
            descripcion: this.descripcion,
            codigo: this.codigo,
            imagen: this.imagen
        }).guardar();
    }

    
}

module.exports = Articulo;