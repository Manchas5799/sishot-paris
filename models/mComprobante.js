const main = require('./');
const cliente = require('./mClientes');
class Factura {
    constructor(datos = "") {
        //super('factura',datos,condition,datos_soli,tables_adi);
        this.id = datos.id;
        this.table_name = 'factura';
        this.total = datos.total;
        this.fecha = datos.fecha;
        this.IVA = datos.IVA || 0;
        this.cliente = datos.CLIENTES_PERSONA_id ? new cliente({ PERSONA_id: datos.CLIENTES_PERSONA_id }) : null;
    }
    async setCliente(id) {
        console.log(id);
        this.cliente = new cliente({ PERSONA_id: id });
        await this.cliente.getOne();
    }
    setTotal(x){
        this.total = x;
    }
    setFecha(x){
        this.fecha = x;
    }
    setIva(x){
        this.IVA = x;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();

        this.setTotal(dato.total);
        this.setFecha(dato.fecha);
        this.setIva(dato.IVA);
        await this.setCliente(dato.CLIENTES_PERSONA_id);
    }
    async guardar() {
        let con = new main(this.table_name, {
            total: this.total,
            IVA: this.IVA,
            CLIENTES_PERSONA_id: this.cliente.persona.id
        });
        this.id = await con.guardar();
        return this.id;
    }
}

module.exports = Factura;