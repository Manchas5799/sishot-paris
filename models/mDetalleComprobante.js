const main = require('./');
const Factura = require('./mComprobante');
const Reserva = require('./mReserva');
class DetalleFactura{
    constructor(datos = "",dataP = null) {
        //super('factura',datos,condition,datos_soli,tables_adi);
        this.id = datos.id;
        this.table_name = 'detalle_factura';
        this.subtotal = datos.subtotal;
        this.fecha = datos.fecha;
        this.factura = datos.FACTURA_id ? new Factura({ id: datos.FACTURA_id }) : null;
        if(dataP == null){
            this.reserva = datos.RESERVACION_id?new Reserva({id: datos.RESERVACION_id}):null;
        }else{
            this.reserva = new Reserva({oldDatos: dataP});
        }
        // this.cliente = datos.CLIENTES_PERSONA_id ? new cliente({ PERSONA_id: datos.CLIENTES_PERSONA_id }) : null;
    }
    async setReserva(id) {
		this.reserva = new Reserva({id});
		await this.reserva.getOne();
    }
    async setFactura(id) {
		this.factura = new Factura({id});
		await this.factura.getOne();
    }
    setSubtotal(x){
        this.subtotal = x;
    }
    setFecha(x){
        this.fecha = x;
    }
    async getOne() {
		let con = new main(this.table_name, {}, { id: this.id });
		let dato = await con.getOne();

		this.setsubtotal(dato.subtotal);
        this.setFecha(dato.fecha);
        await this.setFactura(dato.FACTURA_id);
		await this.setReserva(dato.RESERVACION_id); 
    }
    async getAll(){
		let datos = await new main(this.table_name).getAll();
		let objs = [];
		for (let i = 0; i < datos.length; i++) {
			objs[i] = new DetalleFactura({
				id: datos[i].id,
				RESERVACION_id: datos[i].RESERVACION_id,
				FACTURA_id: datos[i].FACTURA_id,
				subtotal : datos[i].subtotal,
				fecha : datos[i].fecha
			});
			await objs[i].getOne();
		}
		return objs;
	}
    async guardar() {
		let con = new main(this.table_name, {
			RESERVACION_id: this.reserva.id,
			fecha: this.fecha,
            subtotal: this.subtotal,
            FACTURA_id : this.factura.id
		});
		await con.guardar();
	}
}

module.exports = DetalleFactura;