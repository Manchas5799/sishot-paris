const main = require('./');
const Hotel = require('./mHotel');
const TipoHabitacion = require('./mTipoHabitacion');
const EstadoHabitacion = require('./mEstadosHabitaciones');
class Habitacion {
	constructor(datos = "") {
		//super('habitaciones',datos,condition,datos_soli,tables_adi);
		this.table_name = 'habitaciones';
		this.id = datos.id;
		this.hotel = datos.HOTEL_id ? new Hotel({ id: datos.HOTEL_id }) : null;
		this.tipoHabitacion = datos.TIPO_HABITACION_id ? new TipoHabitacion({ id: datos.TIPO_HABITACION_id }) : null;
		this.codigo = datos.codigo;
		this.estado = datos.ESTADOS_id ? new EstadoHabitacion({ id: datos.ESTADOS_id }) : null;
		this.imagen = datos.imagen;
	}
	async getAll() {
		let datos = await new main(this.table_name).getAll();
		let objs = [];
		for (let i = 0; i < datos.length; i++) {
			objs[i] = new Habitacion({
				id: datos[i].id,
				HOTEL_id: datos[i].HOTEL_id,
				TIPO_HABITACION_id: datos[i].TIPO_HABITACION_id,
				codigo: datos[i].codigo,
				ESTADOS_id: datos[i].ESTADOS_id,
				imagen: datos[i].imagen
			});
			await objs[i].getOne();
		}
		return objs;
	}
	async getOne() {
		let con = new main(this.table_name, {}, { id: this.id });
		let dato = await con.getOne();
		// console.log(this);
		await this.setHotel(dato.HOTEL_id);
		await this.setTipoHabitacion(dato.TIPO_HABITACION_id);
		this.setCodigo(dato.codigo);
		await this.setEstado(dato.ESTADOS_id);
		this.setImagen(dato.imagen);
	}
	async setHotel(id) {
		this.hotel = new Hotel({ id });
		await this.hotel.getOne();
	}
	async setTipoHabitacion(id) {
		this.tipoHabitacion = new TipoHabitacion({ id });
		await this.tipoHabitacion.getOne();
	}
	setCodigo(codigo) {
		this.codigo = codigo;
	}
	async setEstado(id) {
		this.estado = new EstadoHabitacion({ id });
		await this.estado.getOne();
	}
	setImagen(imagen) {
		this.imagen = imagen;
	}
	async guardar() {
		return await new main(this.table_name, {
			HOTEL_id: this.hotel.id,
			TIPO_HABITACION_id: this.tipoHabitacion.id,
			codigo: this.codigo,
			ESTADOS_id: this.estado.id,
			imagen: this.imagen
		}).guardar();
	}
	async actualizar(datos) {
		let con = new main(this.table_name, datos, { id: this.id });
		await con.actualizar();
	}
	async eliminar() {
		let con = new main(this.table_name, {}, { id: this.id });
		await con.eliminar();
	}
}

module.exports = Habitacion;