const bd = require('../server/bd');
const main = require('./');
const ciudad = require('./mCiudad');
const pais = require('./mPais');
class Hotel{
    constructor(datos = ""){
        //super('hotel',datos,condition,datos_soli,tables_adi);
        this.table_name = 'hotel';
        this.id = datos.id;
        this.direccion = datos.direccion;
        this.nombre = datos.nombre;
        this.telefono = datos.telefono;
        this.ruc = datos.ruc;
        this.correo = datos.correo;
        this.description = datos.description;
        this.ciudad = datos.CIUDAD_id?new ciudad({id : datos.CIUDAD_id}):0;
        this.pais = datos.CIUDAD_PAIS_id?new pais({id : datos.CIUDAD_PAIS_id}):0;
        this.grupo = datos.grupo;
    }
	async getAll(){
    	let datos = await new main(this.table_name).getAll();
    	let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new Hotel({
				id:datos[i].id,
				nombre : datos[i].nombre,
				telefono: datos[i].telefono,
				ruc : datos[i].ruc,
				correo : datos[i].correo,
				description: datos[i].description,
				CIUDAD_id : datos[i].CIUDAD_id,
				CIUDAD_PAIS_id : datos[i].CIUDAD_PAIS_id
				});
		}
		return objs;
    }
    async getOne(){
    	let con = new main(this.table_name,{},{id :this.id});
    	let dato = await con.getOne();
    	this.setNombre(dato.nombre);
    	this.setDireccion(dato.direccion);
    	this.setTelefono(dato.telefono);
    	this.setRuc(dato.ruc);
    	this.setCorreo(dato.correo);
    	this.setDescription(dato.description);
    	this.setGrupo(dato.grupo);
    	this.setCiudad(dato.CIUDAD_id);
    	this.setPais(dato.CIUDAD_PAIS_id);
    }
    async eliminar(){
    	let con = new main(this.table_name,{},{id : this.id});
    	await con.eliminar();
    }
    async actualizar(datos){
    	let con = new main(this.table_name,datos,{id : this.id});
    	await con.actualizar();
    }
    setId(id){
    	this.id = id;
    }
    setNombre(nombre){
    	this.nombre = nombre;
    }
    setDireccion(dir){
    	this.direccion = dir;
    }
    setTelefono(tel){
    	this.telefono = tel;
    }
    setRuc(n){
    	this.ruc = n;
    }
    setCorreo(t){
    	this.correo = t;
    }
    setDescription(t){
    	this.description = t;
    }
    setGrupo(g){
    	this.grupo = g;
    }
    async setCiudad(id){
    	this.ciudad = new ciudad({id});
    	await this.ciudad.getOne();
    }
    async setPais(id){
    	this.pais = new pais({id});
    	await this.pais.getOne();
    }
    async guardar(){
    	return await new main(this.table_name,{
    		nombre : this.nombre,
    		direccion: this.direccion,
    		telefono: this.telefono,
    		ruc: this.ruc,
    		correo : this.correo,
    		description: this.description,
    		CIUDAD_id : this.ciudad.id,
    		CIUDAD_PAIS_id : this.pais.id,
    		grupo: this.grupo
    	}).guardar();
    }    

    
    
}

module.exports = Hotel;