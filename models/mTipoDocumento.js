const main = require('./')
class TipoDocumento{
    constructor(datos = ""){
        //super('tipo_documento',datos,condition,datos_soli,tables_adi);
        this.table_name = 'tipo_documento';
        this.id = datos.id;
        this.nombre = datos.nombre;
    }
    async getAll(){
    	let datos = await new main(this.table_name).getAll();
    	let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new TipoDocumento({
                id:datos[i].id,
                nombre:datos[i].nombre
            });
		}
		return objs;
    }
    async getOne(){
        let con = new main(this.table_name,{},{id : this.id});
        let dato = await con.getOne();
        this.setid(dato.id);
        this.setnombre(dato.nombre);
    }
    async eliminar(){
        let con = new main(this.table_name,{},{id : this.id});
        await con.eliminar(); 
    }
    async actualizar(datos){
        let con = new main(this.table_name,datos,{id : this.id});
        await con.actualizar(); 
    }
    setid(id){
        this.id = id;
    }
    setnombre(nombre){
        this.nombre = nombre;
    }
    async guardar(){
        return await new main(this.table_name,{
            nombre : this.nombre
        }).guardar();
    }
}

module.exports = TipoDocumento;