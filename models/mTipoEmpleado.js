const bd = require('../server/bd');
const main = require('./')
class TipoEmpleado {
    constructor(datos = ""){
        //super('tipo_empleado',datos,condition,datos_soli,tables_adi);
        this.table_name = 'tipo_empleado';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.sueldo = datos.sueldo
    }
    async getAll(){
    	let datos = await new main(this.table_name).getAll();
    	let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new TipoEmpleado({id:datos[i].id,
											nombre : datos[i].nombre,
											sueldo: datos[i].sueldo});
		}
		return objs;
    }
    async getOne(){
    	let con = new main(this.table_name,{},{id :this.id});
    	let dato = await con.getOne();
    	this.setId(dato.id);
    	this.setSueldo(dato.sueldo);
    	this.setNombre(dato.nombre);
    }
    setId(id){
    	this.id = id;
    }
    setNombre(nombre){
    	this.nombre = nombre;
    }
    setSueldo(sueldo){
    	this.sueldo = sueldo;
    }
    async eliminar(){
    	let con = new main(this.table_name,{},{id : this.id});
    	await con.eliminar();
    }
    async actualizar(datos){
    	let con = new main(this.table_name,datos,{id : this.id});
    	await con.actualizar();
    }
    async guardar(){
    	return await new main(this.table_name,{
    		nombre : this.nombre,
    		sueldo: this.sueldo
    	}).guardar();
    }
}

module.exports = TipoEmpleado;