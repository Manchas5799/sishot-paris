const bd = require('../server/bd');
const main = require('./')
class TipoHabitacion{
    constructor(datos = {}){//condition = "",datos_soli = "*",tables_adi = ""){
        //super('tipo_habitacion',datos,condition,datos_soli,tables_adi);
        this.table_name = 'tipo_habitacion';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.costo1 = datos.costo1;
        this.costo2 = datos.costo2;
        this.costo3 = datos.costo3;
        this.costo4 = datos.costo4;
    }
    async getAll(){
    	let datos = await new main(this.table_name).getAll();
    	let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new TipoHabitacion({id:datos[i].id,
											nombre : datos[i].nombre,
											costo1: datos[i].costo1,
											costo2: datos[i].costo2,
											costo3: datos[i].costo3,
											costo4: datos[i].cost4});
		}
		return objs;
    }
    async getOne(){
    	let con = new main(this.table_name,{},{id :this.id});
    	let dato = await con.getOne();
    	this.setCosto1(dato.costo1);
    	this.setCosto2(dato.costo2);
    	this.setCosto3(dato.costo3);
    	this.setCosto4(dato.costo4);
    	this.setNombre(dato.nombre);
    }
    async eliminar(){
    	let con = new main(this.table_name,{},{id : this.id});
    	await con.eliminar();
    }
    async actualizar(datos){
    	let con = new main(this.table_name,datos,{id : this.id});
    	await con.actualizar();
    }
    setId(id){
    	this.id = id;
    }
    setNombre(nombre){
    	this.nombre = nombre;
    }
    setCosto1(costo){
    	this.costo1 = costo;
    }
    setCosto2(costo){
    	this.costo2 = costo;
    }
    setCosto3(costo){
    	this.costo3 = costo;
    }
    setCosto4(costo){
    	this.costo4 = costo;
    }
    async guardar(){
    	return await new main(this.table_name,{
    		nombre : this.nombre,
    		costo1: this.costo1,
    		costo2: this.costo2,
    		costo3: this.costo3,
    		costo4: this.costo4
    	}).guardar();
    }

}

module.exports = TipoHabitacion;