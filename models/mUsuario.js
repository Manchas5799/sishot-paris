const main = require('./');
const TipoDocumento = require('./mTipoDocumento');
const ciudad = require('./mCiudad');
const pais = require('./mPais');
class Persona{
	constructor(datos = ""){
		//super('persona',datos,condition,datos_soli,tables_adi);
		this.id = datos.id;
		this.table_name= 'persona';
		this.nombres = datos.nombres;
		this.apellidos = datos.apellidos;
		this.correo = datos.correo;
		this.telefono = datos.telefono;
		this.sexo = datos.sexo;
		this.documento = datos.documento;
		this.password = datos.password;
		this.tipoDocumento = datos.TIPO_DOCUMENTO_id?new TipoDocumento({id : datos.TIPO_DOCUMENTO_id}):null;
		this.ciudad = datos.CIUDAD_id?new ciudad({id : datos.CIUDAD_id}):null;
		this.pais = datos.CIUDAD_PAIS_id?new pais({id : datos.CIUDAD_PAIS_id}):null;
	}
	async getAll(){
		let datos = await new main(this.table_name).getAll();
		let objs = [];
		for(let i = 0; i < datos.length;i++){
			objs[i] = new Persona({
				id:datos[i].id,
				nombre : datos[i].nombres,
				apellidos : datos[i].apellidos,
				correo : datos[i].correo,
				telefono: datos[i].telefono,
				sexo : datos[i].sexo,
				documento : datos[i].documento,
				password: datos[i].password,
				TIPO_DOCUMENTO_id : datos[i].TIPO_DOCUMENTO_id,
				CIUDAD_id : datos[i].CIUDAD_id,
				CIUDAD_PAIS_id : datos[i].CIUDAD_PAIS_id
				});
		}
		return objs;
	}
	async getOne(){
		let con = new main(this.table_name,{},{id :this.id});
		let dato = await con.getOne();
		this.setNombre(dato.nombres);
		this.setApellidos(dato.apellidos);
		this.setTelefono(dato.telefono);
		this.setSexo(dato.sexo);
		this.setCorreo(dato.correo);
		this.setDocumento(dato.documento);
		this.setTipoDocumento(dato.TIPO_DOCUMENTO_id);
		this.setCiudad(dato.CIUDAD_id);
		this.setPais(dato.CIUDAD_PAIS_id);
	}
	async eliminar(){
		let con = new main(this.table_name,{},{id : this.id});
		await con.eliminar();
	}
	async actualizar(datos){
		let con = new main(this.table_name,datos,{id : this.id});
		await con.actualizar();
	}
	setId(id){
		this.id = id;
	}
	setNombre(nombre){
		this.nombres = nombre;
	}
	setApellidos(apellidos){
		this.apellidos = apellidos;
	}
	setCorreo(t){
		this.correo = t;
	}
	setTelefono(tel){
		this.telefono = tel;
	}
	setSexo(sexo){
		this.sexo = sexo;
	}
	setDocumento(n){
		this.documento = n;
	}
	setPassword(p){
		this.password = p;
	}
	async setTipoDocumento(id){
		this.tipoDocumento = new TipoDocumento({ id});
		this.tipoDocumento.getOne();

	}
	async setCiudad(id){
		this.ciudad = new ciudad({id});
		this.ciudad.getOne();
	}
	async setPais(id){
		this.pais = new pais({id});
		this.pais.getOne();
	}
	async guardar(){
		let ans = 0;
		try {
			ans = await new main(this.table_name,{
				nombres : this.nombres,
				apellidos : this.apellidos,
				sexo : this.sexo,
				telefono: this.telefono,
				documento : this.documento,
				correo : this.correo,
				password : this.password,
				CIUDAD_id : this.ciudad.id,
				CIUDAD_PAIS_id : this.pais.id,
				TIPO_DOCUMENTO_id : this.tipoDocumento.id
			}).guardar();
		} catch (error) {
			console.log(error);
			throw error;
		}
		return ans||null;
	}  
	async inner(condition,selec,sec_table){
		const datos = await new main(this.table_name,{},condition,selec,sec_table).inner();
		return datos;
	}
}

module.exports = Persona;