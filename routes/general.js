const router = require('express').Router();
const general = require('../controllers/General');
const { login, saveIngreso} = require('../controllers/Sistema');
const passport = require('passport');
//RUTAS
	router.get('/',general.index);
	router.get('/our',general.nosotros);
	router.get('/rooms',general.habitaciones);
	router.get('/galery',general.galeria);
	router.get('/contact',general.contact);

////RUTA DE INGRESO Y REGISTRO para el sistema
	router.get('/registro',general.registro);
	router.post('/registro',passport.authenticate('local.registro', {
            successRedirect: '/',
            failureRedirect: '/registro',
            failureFlash: true
        }),general.saveRegistro);
	/////////////////////////////
	router.get('/sistema/login', login);
	router.post('/sistema/login',saveIngreso);
	///////////////////////
	router.get('/login', general.login);
	router.post('/login',general.saveIngreso);
	router.get('/logout',general.logout);
	/*-----------------------------------*/
	router.get('/habitacion/:id',general.showHabitacion);

///////////////
///////RUTAS PARA LAS VISTAS
/////////////////
router.post('/actions/ciudades/:pais',general.mCiudades);


module.exports = router;