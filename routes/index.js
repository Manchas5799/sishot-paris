const sistema = require('./sistema');
const usuario = require('./usuario');
const general = require('./general');
// const passport = require('passport');
// const router = require('express').Router();
function validate(req,res,next){
	if(req.isAuthenticated()){
		return next();
	}else{
		res.redirect('/sistema/login');
	}
}
function validatePublic(req,res,next){
	if(req.isAuthenticated()){
		return next();
	}else{
		res.redirect('/login');
	}
}
module.exports = app => {
    ///RUTAS PRINCIPALES
    app.use(general);

    ///RUTAS PARA EL SISTEMA
    app.use('/sistema',validate,sistema);
    ///RUTAS PARA LOS USUARIOS
    app.use('/usuario',validatePublic,usuario);
    // app.use(router);
};