const router = require('express').Router();
const usuario = require('../controllers/Usuario');

//RUTAS
router.get('/carrito',usuario.index);
router.post('/carrito/add',usuario.add);
router.get('/carrito/:id/remove',usuario.remover);
router.get('/carrito/reset',usuario.reset);
router.get('/reservar',usuario.reservar);
router.get('/reservas',usuario.showAll);
module.exports = router;