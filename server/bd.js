const mysql = require('mysql');
const { promisify } = require('util');
const {
    database
} = require('../database/db');
const dbb = mysql.createPool(database);
dbb.query = promisify(dbb.query);
module.exports = dbb;