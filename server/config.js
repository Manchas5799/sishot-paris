const path = require('path');
const hbs = require('express-handlebars');
const morgan = require('morgan');
const varEnt = require('../config/var.json');
const flash = require('connect-flash');
///////////////////////////SESSION VARS
const session = require('express-session');
const sessionStore = require('express-mysql-session');/*(session);*/
const passport = require('passport');

const { database } = require('../database/db');

//////////////////////////
const multer = require('multer');
const express = require('express');
const routes = require('../routes');
// require('bootstrap');

const errorHandler = require('errorhandler');
module.exports = app => {
    ///SETTINGS
    require('../models/auth/passport');
    app.set('port', process.env.PORT || varEnt.development.PORT);
    app.set('views', path.join(__dirname, '..'+varEnt.development.VIEWS));
    app.engine('.hbs', hbs({
        defaultLayout: 'main',
        partialsDir: path.join(app.get('views'), 'partials'),
        layoutsDir: path.join(app.get('views'), 'layouts'),
        extname: '.hbs',
        helpers: require('./helpers')
    }));

    app.set('view engine', '.hbs');
    ////MIDLEWARES
    app.use(session({
      secret: 'sishotUser',
      resave: false,
      saveUninitialized: false,
      store: new sessionStore(database)
      // cookie: { secure: true }
    }));
    app.use(flash());
    app.use(morgan('dev'));
    app.use(multer({
        dest: path.join(__dirname, '../public/upload/temp')
    }).single('fileUpload'));
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(express.json());
    app.use(passport.initialize());
    app.use(passport.session());

    app.use((req,res,next) => {
        app.locals.fallido = req.flash('fallido');
        app.locals.exitoso = req.flash('exitoso');
        app.locals.user = req.user;
        app.locals.reservas = req.session.reser;
        next();
    });
    ////RUTAS
    routes(app);

    ///FILES STATIC
    let pub = path.join(__dirname,'../'+varEnt.development.PUBLIC);
    // app.use('/public',express.static(path.join(__dirname,'../'+varEnt.development.PUBLIC)));
    console.log(pub);
    // console.log(path.join(__dirname,'..'+varEnt.development.PUBLIC));
    app.use('/public',express.static(path.join(__dirname,'..'+varEnt.development.PUBLIC)));
    ///ERRORHANDLERS
    if ('development' === app.get('env')) {
        app.use(errorHandler());
    }
    return app;
};